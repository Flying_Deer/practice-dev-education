package com.github.dialogs;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Main {
    public static void main(String [] args)
    {
        int returnValue = 0;
        returnValue = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit?", "Are you sure?", JOptionPane.YES_NO_OPTION);

        if (returnValue == JOptionPane.YES_OPTION)
            JOptionPane.showMessageDialog(null, "Yes was clicked.");
        else if (returnValue == JOptionPane.NO_OPTION)
            JOptionPane.showMessageDialog(null, "No was clicked.");
    }//End of main method
}//End of class