package com.github.dialogs;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ReturningDialogTest {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                final JFrame frame = new JFrame();
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                final JPanel panel = new JPanel();

                JButton btn = new JButton("show dialog");

                panel.add(btn);

                final JLabel lab = new JLabel("");

                panel.add(lab);

                frame.add(panel);

                btn.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        MyDialog diag = new MyDialog(frame);
                        diag.display();

                        String info = diag.getInformation();

                        lab.setText(info);

                        frame.pack();
                    }
                });

                frame.setLocationRelativeTo(null);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}
