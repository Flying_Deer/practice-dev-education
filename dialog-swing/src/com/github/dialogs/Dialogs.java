package com.github.dialogs;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class Dialogs {

    public static void simpleMessage(){
        JOptionPane.showMessageDialog(new JFrame(), "Simple message.");
    }

    public static void warningMessage(){
        JOptionPane.showMessageDialog(new JFrame(), "Warning message.",
                "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public static void errorMessage(){
        JOptionPane.showMessageDialog(new JFrame(), "Error message.",
                "Warning", JOptionPane.ERROR_MESSAGE);
    }

    public static void planeMessage(){
        JOptionPane.showMessageDialog(new JFrame(), "Plain message.",
                "Plain", JOptionPane.PLAIN_MESSAGE);
    }

    public static void infoMessage(){
        JOptionPane.showMessageDialog(new JFrame(),
                "Information message.",
                "Info",
                JOptionPane.INFORMATION_MESSAGE,
                new ImageIcon());
    }

    public static void yesNoCancelOptionDialog(){
        Object[] options = {"Yes option", "No option", "Cancel option"};
        JOptionPane.showOptionDialog(new JFrame(), "Option dialog",
                "Options", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
    }

    public static void yesNoOptionDialog(){
        Object[] options = {"Yes option", "No option"};
        JOptionPane.showOptionDialog(new JFrame(), "Option dialog",
                "Options", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
    }

    public static String comboDialog(){
        Object[] options = {"First option", "Second option", "Third option"};
        return (String) JOptionPane.showInputDialog(new JFrame(), "Combo dialog",
                "Combo", JOptionPane.PLAIN_MESSAGE, null, options, "First option");
    }

    public static String inputDialog(){
        return (String) JOptionPane.showInputDialog(new JFrame(), "Input dialog",
                "Input", JOptionPane.PLAIN_MESSAGE, null, null, "");
    }

    public static Color colorDialog(){
        return JColorChooser.showDialog(new JFrame(), "Select a color", Color.RED);
    }

    public static File fileDialog(){
        JFrame dialogFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Choose file");
        int userSelection = fileChooser.showSaveDialog(dialogFrame);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            System.out.println("Save as file: " + fileToSave.getAbsolutePath());
            return fileToSave;
        }
        return null;
    }
}
