package org.bitbucket.app.http.request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SimpleHttpRequest implements ISimpleHttpRequest{

    private String request;

    public SimpleHttpRequest(String request) {
        this.init(request);
    }

    private void init(String request){
        this.request = request;
    }

    private void setRequest(String request){
        this.init(request);
    }

    private void setRequest(InputStream in) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String result = "";
        String str;
        while ((str = br.readLine()) != null && !str.isEmpty()) {
            result = result.concat(str);
        }
        this.init(result);
    }


    @Override
    public Method getMethod() {
        return null;
    }
}
