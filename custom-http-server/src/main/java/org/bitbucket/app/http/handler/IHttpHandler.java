package org.bitbucket.app.http.handler;

import org.bitbucket.app.http.request.SimpleHttpRequest;
import org.bitbucket.app.http.responce.SimpleHttpResponse;

public interface IHttpHandler {

    void get(SimpleHttpRequest request, SimpleHttpResponse response);

    void post(SimpleHttpRequest request, SimpleHttpResponse response);

}
