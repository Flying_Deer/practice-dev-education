package org.bitbucket.app;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(8080);

        System.out.println("Server has start " + 8080 + ".");

        while (true) {

            Socket socket = serverSocket.accept();
            InputStream in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));

            String outString = "<body>Olololo</body>\n";

            bw.write("HTTP/1.1 200 OK\n\r" +
                    "Content-Length: " + outString.length() + "\n\r" +
                    "Content-Type: text/plain\n\r"
            );


            bw.write("\n\r" + outString + "\n\r");

            System.out.println("Socket has close.");

            bw.flush();
            bw.close();
            br.close();
            socket.close();

        }


    }

}
