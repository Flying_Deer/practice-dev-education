package com.github;

import com.github.models.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentContainer {

    private Student[] students;


    public StudentContainer(Student[] students) {
        this.students = students;
    }

    public StudentContainer() {
        this.students = new Student[0];
    }

    public Student[] getByDepartment(String department){
        if(department == null || department.equals(""))
            throw new IllegalArgumentException();

        List<Student> temp = new ArrayList<>(0);
        for (Student student: students) {
            if(student.getDepartment().equals(department)){
                temp.add(student);
            }
        }
        return (Student[]) temp.toArray();
    }

    public Student[][] sortByCourse(Student[] students){
        return null;
    }

    public Student[][] sortByDepartmentAndCourse(){
        return null;
    }





}
