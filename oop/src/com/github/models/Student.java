package com.github.models;

import java.util.Objects;

public class Student{

    private long id;

    private String firstName;

    private String lastName;

    private String patronymic;

    private int birthYear;

    private String address;

    private String phone;

    private String department;

    private int course;

    private String group;

    public Student(long id, String firstName, String lastName, String patronymic, int birthYear, String address, String phone, String department, int course, String group) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.birthYear = birthYear;
        this.address = address;
        this.phone = phone;
        this.department = department;
        this.course = course;
        this.group = group;
    }

    public Student() {
        this.id = 0;
        this.firstName = "firstName";
        this.lastName = "lastName";
        this.patronymic = "patronymic";
        this.birthYear = 1990;
        this.address = "address";
        this.phone = "phone";
        this.department = "department";
        this.course = 0;
        this.group = "group";
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getDepartment() {
        return department;
    }

    public int getCourse() {
        return course;
    }

    public String getGroup() {
        return group;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id && birthYear == student.birthYear && course == student.course && firstName.equals(student.firstName) && lastName.equals(student.lastName) && patronymic.equals(student.patronymic) && address.equals(student.address) && phone.equals(student.phone) && department.equals(student.department) && group.equals(student.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, patronymic, birthYear, address, phone, department, course, group);
    }
}
