package com.github.models;

import java.util.Objects;

public class Customer {

    private long id;

    private String firstName;

    private String lastName;

    private String patronymic;

    private String creditCard;

    private String bankAccount;

    public Customer(long id, String firstName, String lastName, String patronymic, String creditCard, String bankAccount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.creditCard = creditCard;
        this.bankAccount = bankAccount;
    }

    public Customer() {
        this.id = 0;
        this.firstName = "firstName";
        this.lastName = "lastName";
        this.patronymic = "patronymic";
        this.creditCard = "creditCard";
        this.bankAccount = "bankAccount";
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id && firstName.equals(customer.firstName) && lastName.equals(customer.lastName) && patronymic.equals(customer.patronymic) && creditCard.equals(customer.creditCard) && bankAccount.equals(customer.bankAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, patronymic, creditCard, bankAccount);
    }


}
