package org.bitbucket.example.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/spring-example")
public class ExampleController {

    @GetMapping("/get/{text}")
    public String getHelloWorld(@PathVariable String text) {
        return "<h1>" + text + "</h1>";
    }

}
