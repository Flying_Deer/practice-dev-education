package org.bitbucket.example.services;

import org.bitbucket.example.entities.User;
import org.bitbucket.example.repositories.UserRepository;

import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public long createUser(User user) {
        return userRepository.save(user).getId();
    }

    public User findUserById(long userId) {
        return userRepository.findUserById(userId);
    }

    public User findUserByLogin(String login) {
        return userRepository.findUserByLogin(login);
    }


}
