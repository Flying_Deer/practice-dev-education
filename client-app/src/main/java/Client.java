import java.io.*;
import java.net.Socket;

public class Client {


    private static Socket clientSocket;
    private static BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("localhost", 4040);

                out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                ReadTread readTread = new ReadTread("Read", clientSocket);
                readTread.start();

                System.out.println("Print something for another users...");

                while (true){
                    try {
                        String message = consoleReader.readLine();
                        out.write(message + "\n");
                        out.flush();
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }

            } finally { // в любом случае необходимо закрыть сокет и потоки
                System.out.println("Клиент был закрыт...");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }

}
