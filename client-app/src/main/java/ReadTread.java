import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Objects;

public class ReadTread extends Thread{

    private BufferedReader in;

    public ReadTread(String name, Socket clientSocket) {
        super(name);
        this.in = null;
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run(){
        while (true) {
            try{
                String message = in.readLine();
                if(Objects.nonNull(message)) {
                    System.out.println(message);
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

}
