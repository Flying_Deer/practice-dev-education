const checkAnagrams = function (firstWord, secondWord){
    if(firstWord.length !== secondWord.length){
        return false;
    } else {
        const firstLetters = firstWord.toLowerCase().split('');
        const secondLetters = secondWord.toLowerCase().split('');

        firstLetters.sort();
        secondLetters.sort();

        for(let i = firstLetters.length - 1; i--;){
            if(firstLetters[i] !== secondLetters[i]){
                return false;
            }
        }
        return true;
    }
}

const getWord = function (message){
    let result = prompt(message);
    return result || '';
}

const getWords = function (message){
    let str = prompt(message);
    if(!str){
        return [];
    }
    str = str.trim();
    let words = str.split(/\s+/);
    return words.map((word) => word.trim());
}

const checkAnagramsMany = function (words){
    switch (words.length){
        case 0:
            return 'there is no word';
        case 1:
            return 'there is only one word';
        default:
            let result = null;
            words.forEach(word => {
                if (!checkAnagrams(words[0], word)){
                    result = 'words are not anagrams';
                    return;
                }
            });
            return result || 'words are anagrams';
    }
}

const run = function (){
    // const firstWord = getWord('Enter your first word');
    // const secondWord = getWord('Enter your second word');

    // let result = checkAnagrams(firstWord, secondWord);
    //
    // if(result){
    //     alert('Words are anagrams');
    // } else {
    //     alert('Words are not anagrams');
    // }

    alert(checkAnagramsMany(getWords('Enter your words splitted by spaces.')));

}

run();


