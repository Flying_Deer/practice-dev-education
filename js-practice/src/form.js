
let div = document.getElementById('root')
div.className = 'container';

let form = document.createElement('form');

let login = document.createElement('input');
login.className = 'login';
login.style.outline = 'none';

let password = document.createElement('input');
password.setAttribute('type', 'password');

let button = document.createElement('button');
button.setAttribute('type', 'button');
let textNode = document.createTextNode('Press me');
button.appendChild(textNode);


form.appendChild(login);
form.appendChild(password);
form.appendChild(button);

div.appendChild(form);

button.addEventListener('click', () => {
    if(!login.value || !password.value){
        if(!login.value){
            login.style.borderColor = 'red';
        } else {
            password.style.borderColor = 'red';
        }
        alert('Empty fields');
    } else {
        login.style.borderColor = 'black';
        password.style.borderColor = 'black';
        alert('All is OK.');
    }
});

login.addEventListener('input', e =>{
    if(e.target.value.length < 4){
        login.style.borderColor = 'red';
    } else if(e.target.value.length > 16){
        login.style.borderColor = 'red';
        e.target.value = e.target.value.substr(0, 16);
    } else {
        login.style.borderColor = 'black';
    }
});